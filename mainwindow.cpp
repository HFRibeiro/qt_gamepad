
#include <QtQuick/QQuickView>
#include <QtQuick/QQuickItem>
#include <QUrl>
#include <QDebug>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QQuickView *viewLeft = new QQuickView();
    viewLeft->setSource(QUrl("qrc:/res/virtual_joystick.qml"));

    // transparent background
    //setAttribute(Qt::WA_TranslucentBackground);
    //setStyleSheet("background:transparent;");
    // no window decorations
    //setWindowFlags(Qt::FramelessWindowHint);

    QQuickView *viewRight = new QQuickView();
    viewRight->setSource(QUrl("qrc:/res/virtual_joystick.qml"));

    // Attach to the 'mouse moved' signal
    QQuickItem *rootLeft = viewLeft->rootObject();
    connect(rootLeft,SIGNAL(joystick_moved(double, double)),this,SLOT(joystick_moved_left(double, double)));

    QQuickItem *rootRight = viewRight->rootObject();
    connect(rootRight,SIGNAL(joystick_moved(double, double)),this,SLOT(joystick_moved_right(double, double)));

    // Create a container widget for the QQuickView
    QWidget *leftJoy = QWidget::createWindowContainer(viewLeft, this);
    leftJoy->setMinimumSize(80, 81);
    leftJoy->setMaximumSize(80, 81);
    ui->leftJoyLayout->addWidget(leftJoy);

    QWidget *rightJoy = QWidget::createWindowContainer(viewRight, this);
    rightJoy->setMinimumSize(80, 81);
    rightJoy->setMaximumSize(80, 81);
    ui->rightJoyLayout->addWidget(rightJoy);

}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::joystick_moved_left(double x, double y)
{
    qDebug() << "Left: " << x << ", " << y;
}

void MainWindow::joystick_moved_right(double x, double y)
{
    qDebug() << "Right: " << x << ", " << y;
}

void MainWindow::on_bt_circle_clicked()
{

}

void MainWindow::on_bt_cross_clicked()
{

}

void MainWindow::on_bt_triangle_clicked()
{

}

void MainWindow::on_bt_square_clicked()
{

}

void MainWindow::on_bt_L1_clicked()
{

}

void MainWindow::on_bt_L2_clicked()
{

}

void MainWindow::on_bt_R2_clicked()
{

}

void MainWindow::on_bt_R1_clicked()
{

}

void MainWindow::on_bt_pad_bottom_clicked()
{

}

void MainWindow::on_bt_pad_right_clicked()
{

}

void MainWindow::on_bt_pad_left_clicked()
{

}

void MainWindow::on_bt_pad_up_clicked()
{

}

void MainWindow::on_bt_select_clicked()
{

}

void MainWindow::on_bt_start_clicked()
{

}

void MainWindow::on_bt_options_clicked()
{

}
