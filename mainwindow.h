#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void joystick_moved_left(double x, double y);
    void joystick_moved_right(double x, double y);

    void on_bt_circle_clicked();

    void on_bt_cross_clicked();

    void on_bt_triangle_clicked();

    void on_bt_square_clicked();

    void on_bt_L1_clicked();

    void on_bt_L2_clicked();

    void on_bt_R2_clicked();

    void on_bt_R1_clicked();

    void on_bt_pad_bottom_clicked();

    void on_bt_pad_right_clicked();

    void on_bt_pad_left_clicked();

    void on_bt_pad_up_clicked();

    void on_bt_select_clicked();

    void on_bt_start_clicked();

    void on_bt_options_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
